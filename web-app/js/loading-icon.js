/**
 * Created with IntelliJ IDEA.
 * User: manhdung
 * Date: 9/8/12
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */


$(document).ready(function () {

    window.loadingIconOn = function () {
        $.blockUI({ css:{
            border:'none',
            padding:'15px',
            backgroundColor:'#000',
            '-webkit-border-radius':'10px',
            '-moz-border-radius':'10px',
            opacity:.5,
            color:'#fff'
        } });
    };

    window.loadingIconOff = function () {
        $.unblockUI();
    };
});