// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'groovy.lojzatran.anketa.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'groovy.lojzatran.anketa.UserRole'
grails.plugins.springsecurity.authority.className = 'groovy.lojzatran.anketa.Role'
grails.plugins.springsecurity.requestMap.className = 'groovy.lojzatran.anketa.Requestmap'
grails.plugins.springsecurity.securityConfigType = 'Requestmap'


grails.plugins.springsecurity.facebook.domain.classname = 'groovy.lojzatran.anketa.FacebookUser'

environments {
    test {
        grails.plugins.springsecurity.facebook.appId = '438211699543562'
        grails.plugins.springsecurity.facebook.secret = '9711c2e89f4e03a10d29a21fdcc8d6e8'
    }
    development {
        grails.plugins.springsecurity.facebook.appId = '438211699543562'
        grails.plugins.springsecurity.facebook.secret = '9711c2e89f4e03a10d29a21fdcc8d6e8'
    }
    production {
        grails.plugins.springsecurity.facebook.appId = '393090340746939'
        grails.plugins.springsecurity.facebook.secret = '5b4c91053a4870784c1da186c417dcc3'
    }
}

grails.plugins.springsecurity.roleHierarchy = '''
   ROLE_ADMIN > ROLE_USER
'''
