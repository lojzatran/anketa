import com.mongodb.util.JSON
import groovy.lojzatran.anketa.FacebookUser
import groovy.lojzatran.anketa.Option
import groovy.lojzatran.anketa.Question

/**
 * Created by IntelliJ IDEA.
 * User: Lojza
 * Date: 31.7.12
 * Time: 9:56
 * Protetika 2.0
 */
class FacebookServiceTests extends GroovyTestCase {
    def facebookService
    def facebookAuthService
    def questionService
    def grailsApplication

    List<FacebookUser> testFacebookUserList = []

    @Override
    protected void tearDown() {
        super.tearDown()    //To change body of overridden methods use File | Settings | File Templates.
        deleteFacebookTestUsers()
    }

    void testGetAppAccessToken() {
        String appAccessToken = facebookAuthService.getAppAccessToken()
        assertNotNull(appAccessToken)
        println "app access token " + appAccessToken
    }

    void testBuildUrlForPostQuestion() {
        FacebookUser fbUser = getTestFacebookUser()

        Question testQuestion = new Question(text: "test")
        Option optionA = new Option(text: "optionA")
        Option optionB = new Option(text: "optionB")
        testQuestion.addToOptionList(optionA)
        testQuestion.addToOptionList(optionB)
        testQuestion.save()

        String finalUrl = facebookService.buildUrlForPostQuestion(fbUser, testQuestion)
        String assertUrl = grailsApplication.config.grails.facebook.user.question.post + "?access_token=${fbUser.accessToken}&question=${testQuestion.text}&options=[\"${optionA.text}\",\"${optionB.text}\"]"
        assert assertUrl == finalUrl
    }

    void testPublishQuestion() {
        Question testQuestion = new Question(text: "test question")
        Option testOption1 = new Option(text: "test option 1")
        Option testOption2 = new Option(text: "test option 2", votes: 2)
        Option testOption3 = new Option(text: "test option 3", votes: 3)
        testQuestion.addToOptionList(testOption1)
        testQuestion.addToOptionList(testOption2)
        testQuestion.addToOptionList(testOption3)
        FacebookUser facebookUser = getTestFacebookUser()

        String questionId = facebookService.publishQuestion(testQuestion, facebookUser)
        assertNotNull(questionId)
        println "question id " + questionId

        questionId = questionService.publishToFacebook(testQuestion, facebookUser)
        assertNotNull(questionId)
        println "question id " + questionId
    }

    FacebookUser getTestFacebookUser(long idx = 0) {
        FacebookUser testFacebookUser
        if (idx + 1 <= testFacebookUserList.size()) {
            testFacebookUser = testFacebookUserList[idx]
        } else {
            testFacebookUser = createTestFacebookUser()
        }
        return testFacebookUser
    }

    FacebookUser createTestFacebookUser() {
        String appId = grailsApplication.config.grails.plugins.springsecurity.facebook.appId
        String name = "testUser"
        String permissions = "email,user_questions,publish_stream"
        String accessToken = facebookAuthService.appAccessToken
        String finalUrlString = "https://graph.facebook.com/${appId}/accounts/test-users?installed=true&name=${name}&locale=en_US&permissions=${permissions}&method=post&access_token=${accessToken}"
        URL finalUrl = new URL(finalUrlString)
        String responseText = finalUrl.text
        def testUserJSON = JSON.parse(responseText)
        FacebookUser testFacebookUser = new FacebookUser()
        testFacebookUser.setUid(testUserJSON.id as Long)
        testFacebookUser.setAccessToken(testUserJSON.access_token)
        testFacebookUserList.add(testFacebookUser)
        return testFacebookUser
    }

    void deleteFacebookTestUsers() {
        testFacebookUserList.each { testUser ->
            String finalUrlString = "https://graph.facebook.com/${testUser.uid}?method=delete&access_token=${testUser.accessToken}"
            URL finalUrl = new URL(finalUrlString)
            assert "true" == finalUrl.text.toLowerCase()
        }
    }

}
