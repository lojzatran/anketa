package groovy.lojzatran.anketa

class OptionService {

    def springSecurityService

    Option get(String id) {
        return Option.findByIdAndDeleted(id, false)
    }

    Answer vote(Option option) {
        User currentUser = springSecurityService.currentUser as User
        Answer answer = new Answer()
        Question question = Question.get(option.question.id)
        if (!question.multipleVotes &&
                Answer.findByQuestionAndUser(question, currentUser)) {
            answer.errors.reject("multiple.vote.error", "Můžete u této otázky hlasovat jen jednou!")
        } else {
            answer.setOption(option)
            answer.setQuestion(question)
            answer.setUser(currentUser)
            answer.save()
            option.votes++
            option.save()
        }
        return answer
    }


}
