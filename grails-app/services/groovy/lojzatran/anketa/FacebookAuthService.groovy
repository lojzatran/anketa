package groovy.lojzatran.anketa

import com.the6hours.grails.springsecurity.facebook.FacebookAuthToken

class FacebookAuthService {

    def facebookService
    def grailsApplication

    FacebookUser create(FacebookAuthToken token) {

        FacebookUser facebookUser = new FacebookUser()
        facebookUser.uid = token.uid
        facebookUser.accessToken = token.accessToken.accessToken

        User appUser = new User()
        prepopulateAppUser(appUser, token)

        User.withTransaction {
            appUser.save(flush: true, failOnError: true)
        }

        facebookUser.user = appUser

        FacebookUser.withTransaction {
            facebookUser.save(flush: true, failOnError: true)
        }
        createRoles(facebookUser)

        return facebookUser
    }

    void prepopulateAppUser(User user, FacebookAuthToken token) {
        user.username = facebookService.getUsername(token.accessToken.accessToken)
        user.password = token.accessToken.accessToken
        user.email = facebookService.getEmail(token.accessToken.accessToken)
    }

    void createRoles(FacebookUser facebookUser) {
        Role facebookUserRole = Role.findByAuthority("ROLE_USER")
        new UserRole(user: facebookUser.user, role: facebookUserRole).save()
    }

    /**
     * get facebook app access token
     * @see {@link https://developers.facebook.com/docs/authentication/applications/}
     * @return
     */
    String getAppAccessToken() {
        String clientId = grailsApplication.config.grails.plugins.springsecurity.facebook.appId
        String clientSecret = grailsApplication.config.grails.plugins.springsecurity.facebook.secret
        String finalUrlString = "https://graph.facebook.com/oauth/access_token?client_id=${clientId}&client_secret=${clientSecret}&grant_type=client_credentials"
        URL url = new URL(finalUrlString)
        String accessToken = url.text - "access_token="
        return accessToken
    }


}
