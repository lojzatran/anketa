package groovy.lojzatran.anketa

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONElement

class FacebookService {

    def grailsApplication

    private JSONElement getUserJSON(String accessToken) {
        String url = grailsApplication.config.grails.facebook.user.current + accessToken
        JSONElement userInfoJSON = JSON.parse(url.toURL().text)
        return userInfoJSON
    }

    String getEmail(FacebookUser facebookUser) {
        JSONElement userInfoJSON = getUserJSON(facebookUser.accessToken)
        String email = userInfoJSON.email
        return email
    }

    String getEmail(String accessToken) {
        JSONElement userInfoJSON = getUserJSON(accessToken)
        String email = userInfoJSON.email
        return email
    }

    String getUsername(String accessToken) {
        JSONElement userInfoJSON = getUserJSON(accessToken)
        String username = userInfoJSON.name
        return username
    }

    /**
     * publish a Facebook Question to Facebook
     * @param question question with options to publish
     * @param fbUser if null, then it's current user
     * @return question id from Facebook
     */
    String publishQuestion(Question question, FacebookUser fbUser) {
        String questionFbId = ""
        if (fbUser) {
            String finalUrlString = buildUrlForPostQuestion(fbUser, question)
            URL finalUrl = new URL(finalUrlString)
            def connection = finalUrl.openConnection()
            connection.setRequestMethod("POST")
            connection.doOutput = true
            connection.connect()
            String responseText = connection.content.text
            def responseJSON = JSON.parse(responseText)
            questionFbId = responseJSON.id
        }
        return questionFbId
    }

    private String buildUrlForPostQuestion(FacebookUser fbUser, Question question) {
        String postUrl = grailsApplication.config.grails.facebook.user.question.post
        String accessToken = fbUser.accessToken
        String questionText = question.text.replace(" ", "_")
        String options = question.optionList.inject("[") {init, option ->
            init += '"' + option.text.replace(" ", "_") + '",'
        }
        options += "]"
        options = options.reverse().replaceFirst(",", "").reverse()
        String finalUrl = "${postUrl}?access_token=${accessToken}&question=${questionText}&options=${options}"
        return finalUrl
    }
}
