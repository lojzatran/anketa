package groovy.lojzatran.anketa

class QuestionService {

    def springSecurityService
    def facebookService

    Question save(Question question) {
        question.setUser(springSecurityService.currentUser as User)
        question.save()
        return question
    }

    Question get(String id) {
//        ObjectId objectId = new ObjectId(id)
        return Question.findByIdAndDeleted(id, false)
    }

    Question update(String id, Map params) {
        Question question = Question.findByIdAndDeleted(id, false)
        question.properties['text', 'optionList'] = params
        question.save()
        return question
    }

    List<Question> list(int max = 100, int offset = 0) {
        return Question.findAllByDeleted(false, [max: max, offset: offset])
    }

    long count() {
        return Question.countByDeleted(false)
    }

    String publishToFacebook(Question question, FacebookUser fbUser = null) {
        if (!fbUser) {
            User currentUser = springSecurityService.currentUser as User
            fbUser = FacebookUser.findByUser(currentUser)
        }
        String questionId = facebookService.publishQuestion(question, fbUser)
        return questionId
    }

    String publishToFacebook(String questionId, FacebookUser fbUser = null) {
        Question question = get(questionId)
        return publishToFacebook(question, fbUser)
    }

}
