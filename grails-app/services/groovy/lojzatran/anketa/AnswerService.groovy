package groovy.lojzatran.anketa

class AnswerService {

    def springSecurityService

    List<Answer> list(User user = null) {
        if (!user) {
            user = springSecurityService.currentUser as User
        }
        return Answer.findAllByUserAndDeleted(user, false)
    }

    long count(User user = null) {
        if (!user) {
            user = springSecurityService.currentUser as User
        }
        return Answer.countByDeletedAndUser(false, user)
    }
}
