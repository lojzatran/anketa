package groovy.lojzatran.anketa

class Option {
    String id

    String text

    Long votes = 0

    static belongsTo = [question: Question]

    boolean deleted = false

    static constraints = {
    }
}
