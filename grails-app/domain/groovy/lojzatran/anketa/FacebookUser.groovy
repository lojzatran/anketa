package groovy.lojzatran.anketa

import com.the6hours.grails.springsecurity.facebook.FacebookUserDomain

class FacebookUser implements FacebookUserDomain {
    String id

    long uid
    String accessToken

    static belongsTo = [user: User]

    boolean deleted = false

    static mapping = {
        tablePerHierarchy false
    }

    static constraints = {
    }

    def beforeInsert() {
    }

    def beforeUpdate() {
    }

    UserRole getUserRole() {
        return UserRole.findByUser(this.user)
    }

    String getUsername() {
        return user.username
    }
}
