package groovy.lojzatran.anketa

class Answer {

    String id

    User user
    Option option
    Question question

    boolean deleted = false

    static mapping = {
    }

    static constraints = {
    }
}
