package groovy.lojzatran.anketa

class Question {

    String id
    String text

    boolean multipleVotes = false   //true pokud lze volit vícekrát

    static belongsTo = [user: User]

    List<Option> optionList = []
    static hasMany = [optionList: Option]

    boolean deleted = false

    static mapping = {
    }

    static constraints = {
    }
}
