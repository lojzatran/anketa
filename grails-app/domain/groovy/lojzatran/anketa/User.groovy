package groovy.lojzatran.anketa

class User {

    String id

    transient springSecurityService

    String username
    String password
    String email
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    boolean deleted = false

    static constraints = {
        username blank: false, unique: true
        password blank: false
        email email: true, unique: true
    }

    static mapping = {
//        id type: org.bson.types.ObjectId, class: org.bson.types.ObjectId
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }
}
