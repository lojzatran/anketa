import ApplicationContextHolder as AppCtx

import grails.util.Environment

class UrlMappings {

    static mappings = {

        if (Environment.currentEnvironment != Environment.TEST) {
            for (controller in AppCtx.grailsApplication.controllerClasses) {  // AppCtx - viz článek
                def cName = controller.logicalPropertyName
                def packageName = controller.packageName
                if (packageName.contains(".admin") || packageName.contains(".springsecurity")) {
                    "/admin/${cName}/$action?/$id?"(controller: cName) {
                        constraints {
                        }
                    }
                } else {
                    "/${cName}/$action?/$id?"(controller: cName) {
                        constraints {
                            // apply constraints here
                        }
                    }
                }
            }
        }

        "/"(view: "/index")
        "500"(view: '/error')

        "/backbone"(view: "/pokus")
    }
}
