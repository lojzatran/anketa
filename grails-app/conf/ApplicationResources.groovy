modules = {
    application {
        resource url: 'js/application.js'
    }

    "loading-icon" {
        resource url: 'js/lib/jquery.blockUI.js'
        resource url: 'js/loading-icon.js'
    }
}