hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        grails {
            mongo {
                host = "localhost"
                port = 27017
                username = "anketa"
                password = "anketa"
                show_sql = true
                format_sql = true
                databaseName = "anketa"
            }
        }
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
        }
    }
    test {
        dataSource {
            dbCreate = "create-drop"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE"
        }
    }
    production {
        grails {
            mongo {
                host = "localhost"
                port = 27017
                username = "anketa"
                password = "anketa"
                show_sql = true
                format_sql = true
            }
        }
        dataSource {
            dbCreate = "create-drop"
            databaseName = "anketa"
//            url = "jdbc:h2:prodDb;MVCC=TRUE"
            pooled = true
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis = 1800000
                timeBetweenEvictionRunsMillis = 1800000
                numTestsPerEvictionRun = 3
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true
                validationQuery = "SELECT 1"
            }
        }
    }
}

