import grails.util.Environment
import groovy.lojzatran.anketa.Requestmap
import groovy.lojzatran.anketa.Role
import groovy.lojzatran.anketa.User
import groovy.lojzatran.anketa.UserRole

class BootStrap {

    def mongo
    def grailsApplication

    def init = { servletContext ->

//        if (Environment.current.name.toLowerCase() == 'test' ||
//                Environment.current.name.toLowerCase() == 'development') {  // mazání databáze
//            def anketaDb = mongo.getDB("anketa")
//            Set<String> colls = anketaDb.getCollectionNames();
//            for (String s : colls) {
//                if (!s.startsWith("system.")) { // systémové nemažeme
//                    def coll = anketaDb.getCollectionFromString(s)
//                    coll?.drop()
//                }
//            }
//        }

        Role userRole = Role.findByAuthority("ROLE_USER") ?: new Role(authority: "ROLE_USER").save()
        Role adminRole = Role.findByAuthority("ROLE_ADMIN") ?: new Role(authority: "ROLE_ADMIN").save()

        User testUser1 = User.findByUsername("testUser1")
        if (!testUser1) {
            testUser1 = new User()
            testUser1.setUsername("testUser1")
            testUser1.setPassword("password1")
            testUser1.setEmail("testUser1@test.com")
            testUser1.save()
        }

        User admin = User.findByUsername("admin")
        if (!admin) {
            admin = new User()
            admin.setUsername("admin")
            admin.setPassword("admin")
            admin.setEmail("admin@test.com")
            admin.save()
        }

//        FacebookUser facebookUser1 = FacebookUser.findByUser(User.findByUsername("facebookUser1"))
//        if (!facebookUser1) {
//            facebookUser1 = new FacebookUser()
//            facebookUser1.setUid(new Date().time)
//            facebookUser1.setAccessToken("test")
//            facebookUser1.setUser(new User(email: "facebookUser1@test.com", username: "facebookUser1", password: "password1").save())
//            facebookUser1.save()
//        }

        UserRole testUser1Role = UserRole.findByUserAndRole(testUser1, userRole) ?: new UserRole(user: testUser1, role: userRole).save()
        UserRole adminUserRole = UserRole.findByUserAndRole(admin, adminRole) ?: new UserRole(user: admin, role: adminRole).save()
//        UserRole facebookUserRole = UserRole.findByUserAndRole(facebookUser1.user, userRole) ?: new UserRole(user: facebookUser1.user, role: userRole).save()

        setRequestmaps()

    }

    def destroy = {}

    private void setRequestmaps() {
        if (Requestmap.count == 0) {
            new Requestmap(url: '/login/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
            new Requestmap(url: '/', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
            new Requestmap(url: '/home/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
            new Requestmap(url: '/survey/', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
            new Requestmap(url: '/survey/list', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
            new Requestmap(url: '/**', configAttribute: 'ROLE_USER').save()
            new Requestmap(url: '/admin', configAttribute: 'ROLE_ADMIN').save()
            new Requestmap(url: '/admin/**', configAttribute: 'ROLE_ADMIN').save()
        }
    }
}
