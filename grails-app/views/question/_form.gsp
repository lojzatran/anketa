<%@ page import="groovy.lojzatran.anketa.Option; groovy.lojzatran.anketa.Question" %>

<g:set var="optionsCount" value="${questionInstance?.optionList?.size() ?: 2}" scope="flash"/>

<div class="fieldcontain ${hasErrors(bean: questionInstance, field: 'text', 'error')} ">
    <label for="text">
        <g:message code="question.text.label" default="Text"/>

    </label>
    <g:textField name="text" value="${questionInstance?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: questionInstance, field: 'optionList', 'error')} ">
    <label for="option-list">
        <g:message code="question.optionList.label" default="Option List"/>

    </label>

    <div id="option-list">
        <g:set var="optionList"
               value="${questionInstance?.optionList?.isEmpty() ? [new Option(), new Option()] : questionInstance.optionList}"/>

        <g:each in="${optionList}" var="option" status="i">
            <div class="fieldcontain">
                <label for="text">
                    <g:message code="option.text.label" default="Text"/>

                </label>
                <g:textField id="text" name="optionList[${i}].text"
                             value="${option?.text}"/>
            </div>
        </g:each>
    </div>

    <g:remoteLink controller="question" action="addOption"
                  onSuccess="\$('#option-list').append(data); ">
        ${message(code: 'default.add.label', args: [message(code: 'option.label', default: 'Option')])}
    </g:remoteLink>

</div>