<div id="barCore-div"></div>
<gvisualization:barCoreChart columns="${[['string', 'Question'], ['number', 'Votes']]}" elementId="barCore-div"
                             hAxis="${new Expando(['title': 'vote'])}" dynamicLoading="${true}"
                             vAxis="${new Expando(['title': 'option'])}" ready="loadingIconOff" error="loadingIconOff"
                             data="${optionList.collect {[it.text, it.votes]}}"/>