<%@ page import="groovy.lojzatran.anketa.Question" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="list-question" class="content scaffold-list" role="main">
    <h1>
        <g:message code="default.list.label" args="[entityName]"/>
    </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <br/>
    <br/>

    <div class="row-fluid">

        <div class="span2">
            <g:link class="btn btn-primary" action="create">
                <g:message code="default.new.label" args="[entityName]"/>
            </g:link>
        </div>

    </div>
    <g:if test="${questionList}">
        <table class="table table-striped">
            <thead>
            <tr>
                <th><g:message code="question.order.label" default="Order"/></th>

                <g:sortableColumn property="text" title="${message(code: 'question.text.label', default: 'Text')}"/>

                <th><g:message code="question.optionList.label" default="Options"/></th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${questionList}" status="i" var="questionInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td>
                        <g:link controller="question" action="show"
                                params="[id: questionInstance.id]">
                            ${i + 1}.
                        </g:link>
                    </td>

                    <td>
                        <g:link controller="question" action="show"
                                params="[id: questionInstance.id]">
                            ${fieldValue(bean: questionInstance, field: "text")}
                        </g:link>
                    </td>

                    <g:set var="optionLetter" value="${"a".toCharacter()}"/>
                    <g:each in="${questionInstance?.optionList}" var="option">
                        <td>
                            <b>${optionLetter}.</b>
                            ${option.text}
                        </td>
                        <g:set var="optionLetter" value="${optionLetter.next()}"/>
                    </g:each>

                </tr>
            </g:each>
            </tbody>
        </table>
    </g:if>

    <div class="pagination">
        <g:paginate total="${questionCount}"/>
    </div>
</div>
</body>
</html>
