<%@ page import="groovy.lojzatran.anketa.User; org.springframework.security.core.context.SecurityContextHolder; groovy.lojzatran.anketa.Question" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div id="show-question" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>

    <div class="alert alert-success" id="message-div"
         <g:if test="${!flash.message}">style="display: none;"</g:if>
         role="status">
        ${flash?.message}
    </div>

    <ol class="property-list question">

        <g:if test="${questionInstance?.text}">
            <li class="fieldcontain">
                <span id="text-label" class="property-label"><g:message code="question.text.label"
                                                                        default="Text"/></span>

                <span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${questionInstance}"
                                                                                        field="text"/></span>

            </li>
        </g:if>

        <g:if test="${questionInstance?.optionList}">
            <li class="fieldcontain">
                <span id="optionList-label" class="property-label">
                    <g:message code="question.optionList.label" default="Option List"/>
                </span>

                <g:set var="optionLetter" value="${"a".toCharacter()}"/>

                <g:each in="${questionInstance.optionList}" var="option" status="i">
                    <br/>
                    <b>${optionLetter}.</b>
                    <span class="property-value" aria-labelledby="optionList-label">
                        ${option?.text?.encodeAsHTML()}
                    </span>
                    <g:remoteLink controller="option" action="vote" params="[id: option.id]" before="loadingIconOn();"
                                  onFailure="loadingIconOff(); \$('#message-div').removeClass('alert-success').addClass('alert-error').show();"
                                  onSuccess="\$('#message-div').removeClass('alert-error').addClass('alert-success');"
                                  class="btn btn-mini btn-primary"
                                  update="[success: 'vote-chart', failure: 'message-div']">
                        Vote
                    </g:remoteLink>

                    <g:set var="optionLetter" value="${optionLetter.next()}"/>
                </g:each>

            </li>
        </g:if>

    </ol>


    <fieldset class="buttons">
        <g:form>
            <g:remoteLink controller="question" action="publishToFacebook" params="[questionId: questionInstance.id]"
                          before="loadingIconOn();" class="btn btn-info"
                          onComplete="\$('#message-div').show();loadingIconOff();"
                          update="[success: 'message-div', failure: '']">
                <g:message code="question.publishToFb.label" default="Publish to Facebook"/>
            </g:remoteLink>  <br/>

            <cst:printIfCurrentUser user="${questionInstance.user}">
                <g:hiddenField name="id" value="${questionInstance?.id}"/>
                <g:link class="btn btn-info" action="edit" id="${questionInstance?.id}">
                    <g:message
                            code="default.button.edit.label"
                            default="Edit"/></g:link>
                <g:actionSubmit class="btn btn-danger" action="delete"
                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>

            </cst:printIfCurrentUser>
        </g:form>
    </fieldset>

    <div id="vote-chart">
        <g:if test="${optionList}">
            <g:render template="chart" model="[optionList: optionList]"/>
        </g:if>
    </div>

</div>
</body>
</html>
