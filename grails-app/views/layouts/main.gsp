<!DOCTYPE html>

<html lang="en">
<head>
    <title><g:layoutTitle default="Anketní systém"/></title>
    <gvisualization:apiImport/>
    <r:require modules="bootstrap, loading-icon"/>
    <r:layoutResources/>

</head>

<body>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
        <ul class="nav">
            <li <g:if test="${params.controller == 'home' || !params.controller}">class="active"</g:if>>
                <g:link controller="home">Home</g:link>
            </li>

            <sec:ifLoggedIn>
                <li <g:if test="${params.controller == 'question'}">class="active"</g:if>>
                    <g:link controller="question" action="list">
                        My surveys
                    </g:link>
                </li>
                <li <g:if test="${params.controller == 'answer'}">class="active"</g:if>>
                    <g:link controller="answer" action="list">My answers</g:link>
                </li>


                </ul>
                <ul class="nav pull-right">

                    <li class="pull-right">
                        <g:link controller="logout">Logout</g:link>
                    </li>
                </ul>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                </ul>
                <ul class="nav pull-right">
                    <li style="margin-top: 9px;">
                        <cst:fbConnect contextPath="${application.contextPath}"
                                       redirectUrl="home"
                                       permissions="email, user_questions, publish_stream"/> with permissions: email
                    </li>
                </ul>
            </sec:ifNotLoggedIn>
        </div>
    </div>
</div>

<div class="container">
    <g:layoutBody/>
</div>


<r:layoutResources/>

</body>
</html>