<%@ page import="groovy.lojzatran.anketa.Option" %>

<g:set var="optionsCount" value="${optionsCount + 1}" scope="flash"/>

<div class="fieldcontain">
    <label for="text">
        <g:message code="option.text.label" default="Text"/>

    </label>
    <g:textField id="text" name="optionList[${optionsCount}].text" value="${optionInstance?.text}"/>
</div>


