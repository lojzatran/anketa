<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'answer.label', default: 'Answer')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="list-answer" class="content scaffold-list" role="main">
    <h1>
        <g:message code="default.list.label" args="[entityName]"/>
    </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <br/>
    <br/>

    <g:if test="${answerList}">
        <table class="table table-striped">
            <thead>
            <tr>
                <th><g:message code="answer.order.label" default="Order"/></th>

                <g:sortableColumn property="text" title="${message(code: 'question.text.label', default: 'Question')}"/>

                <th><g:message code="answer.text.label" default="My vote"/></th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${answerList}" status="i" var="answerInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td>
                        <g:link controller="answer" action="show"
                                params="[id: answerInstance.id]">
                            ${i + 1}.
                        </g:link>
                    </td>

                    <td>
                        <g:link controller="question" action="show"
                                params="[id: answerInstance.option?.question?.id]">
                            ${fieldValue(bean: answerInstance.option?.question, field: "text")}
                        </g:link>
                    </td>


                    <td>
                        ${answerInstance.option?.text}
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </g:if>
    <g:else>
        No answers yet.
    </g:else>

    <div class="pagination">
        <g:paginate total="${answerCount}"/>
    </div>
</div>

</body>
</html>