package groovy.lojzatran.anketa

class OptionController {
    def optionService
    def questionService

    def index() {
        redirect controller: "question", action: "index"
    }

    def vote() {
        Option option = optionService.get(params.id)
        Answer answer = optionService.vote(option)
        if (answer.hasErrors()) {
            render status: 400, text: answer.errors.getAllErrors().collect {it.defaultMessage}.join(", ")
        } else {
            List<Option> optionList = questionService.get(option.question.id).optionList
            render template: "/question/chart", model: [optionList: optionList]
        }
    }
}
