package groovy.lojzatran.anketa

class AnswerController {
    def answerService

    static defaultAction = "list"

    def list() {
        List<Answer> answerList = answerService.list()
        long answerCount = answerService.count()
        render view: "list", model: [answerList: answerList, answerCount: answerCount]
    }
}
