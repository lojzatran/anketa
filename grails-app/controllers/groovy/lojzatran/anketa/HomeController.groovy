package groovy.lojzatran.anketa

class HomeController {

    def index() {
        redirect url: "/"
    }
}
