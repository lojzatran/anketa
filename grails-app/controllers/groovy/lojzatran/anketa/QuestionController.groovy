package groovy.lojzatran.anketa

import org.springframework.dao.DataIntegrityViolationException

class QuestionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def springSecurityService

    def questionService

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        int max = Math.min(params.max ? params.int('max') : 10, 100)
        int offset = params.offset ? params.int('offset') : 0
        List<Question> questionList = questionService.list(max, offset)
        int questionCount = questionService.count()
        [questionList: questionList, questionCount: questionCount]
    }

    def create() {
        Question questionInstance = new Question(params)
        [questionInstance: questionInstance]
    }

    def save() {
        Question questionInstance = new Question(params)
        questionService.save(questionInstance)
        if (!questionInstance.id) {
            render(view: "create", model: [questionInstance: questionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'question.label', default: 'Question'), questionInstance.id])
        redirect(action: "show", id: questionInstance.id)
    }

    def show() {
        Question questionInstance = questionService.get(params.id)
        if (!questionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
            redirect(action: "list")
            return
        }

        [questionInstance: questionInstance, optionList: questionInstance.optionList]
    }

    def edit() {
        def questionInstance = questionService.get(params.id)
        if (!questionInstance || questionInstance.user != springSecurityService.currentUser) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
            redirect(action: "list")
            return
        }

        [questionInstance: questionInstance]
    }

    def update() {
        def questionInstance = questionService.get(params.id)
        if (!questionInstance || questionInstance.user != springSecurityService.currentUser) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (questionInstance.version > version) {
                questionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'question.label', default: 'Question')] as Object[],
                        "Another user has updated this Question while you were editing")
                render(view: "edit", model: [questionInstance: questionInstance])
                return
            }
        }

        questionInstance = questionService.update(params.id, params)

        if (questionInstance.hasErrors()) {
            render(view: "edit", model: [questionInstance: questionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'question.label', default: 'Question'), questionInstance.id])
        redirect(action: "show", id: questionInstance.id)
    }

    def delete() {
        def questionInstance = Question.get(params.id)
        if (!questionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
            redirect(action: "list")
            return
        }

        try {
            questionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'question.label', default: 'Question'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'question.label', default: 'Question'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def addOption() {
        int optionsCount = flash.optionsCount as int
        render template: "/option/form", model: [optionsCount: optionsCount]
    }

    def publishToFacebook() {
        String questionId = params.questionId
        String facebookId = questionService.publishToFacebook(questionId)
        if (facebookId) {
            render status: 200, text: "Your question has been published with id ${facebookId}"
        } else {
            render status: 400, text: "Error occurs, please try again latter."
        }
    }
}
